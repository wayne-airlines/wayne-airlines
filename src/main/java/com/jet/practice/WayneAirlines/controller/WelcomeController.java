package com.jet.practice.WayneAirlines.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/wayne-airlines")
public class WelcomeController {

	@RequestMapping("/")
	public String showLogin() {
		return "login.html";
	}

	@RequestMapping("/sidebar")
	public String showSidebar() {
		return "sidebar.html";
	}

}
