package com.jet.practice.WayneAirlines.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.captcha.botdetect.web.servlet.SimpleCaptcha;
import com.jet.practice.WayneAirlines.util.RequestUtil;
import com.jet.practice.WayneAirlines.vo.LoginAuthVO;
import com.jet.practice.WayneAirlines.vo.LoginVO;

@Controller
@RequestMapping("/wayne-airlines/captcha")
public class CaptchaController {

	@RequestMapping(value = "/validate", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> validateLogin(HttpServletRequest request, HttpServletResponse response,
			@RequestBody LoginVO loginVO) {
		System.out.println("In /validate controller");
		System.out.println(loginVO.toString());

		SimpleCaptcha captcha = SimpleCaptcha.load(request);
		boolean isHuman = captcha.validate(loginVO.getCaptchaVO().getCaptchaCode(),
				loginVO.getCaptchaVO().getCaptchaId());
		try {
			if (isHuman) {
				LoginAuthVO authVO = new LoginAuthVO();
				authVO.setUsername(loginVO.getUsername());
				authVO.setPassword(loginVO.getPassword());
				String token = RequestUtil.postToAuthToken(authVO);
				if (token.isEmpty()) {
					return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No Token");
				} else {
					if (token.equals("401")) {
						return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User not valid");
					}
				}
				System.out.println("captcha pass");
				return ResponseEntity.status(HttpStatus.OK).body(token);
			} else {
				// TODO: redirect to login page. show error
				System.out.println("captcha failed");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Captcha Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
		}
	}

}
