package com.jet.practice.WayneAirlines.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jet.practice.WayneAirlines.util.RequestUtil;
import com.jet.practice.WayneAirlines.vo.Airport;

@Controller
@RequestMapping("/wayne-airlines")
public class AirportController {
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/airlines")
	public void showAllAirports() {
		
	}

}
