package com.jet.practice.WayneAirlines;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.jet.practice.WayneAirlines.util.RequestUtil;
import com.jet.practice.WayneAirlines.vo.LoginAuthVO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WayneAirlinesApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void testAuth() {
		LoginAuthVO loginAuthVO =new LoginAuthVO();
		loginAuthVO.setUsername("j.torres");
		loginAuthVO.setPassword("password1");
		System.out.println((RequestUtil.postToAuthToken(loginAuthVO)));
		assertNotNull(RequestUtil.postToAuthToken(loginAuthVO));
	}

	@Test
	public void testAirports() throws JsonParseException, JsonMappingException, IOException {
	
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGenericGetRequest() throws JsonParseException, JsonMappingException, IOException {
		
	}

}
